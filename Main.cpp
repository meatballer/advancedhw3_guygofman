#include <condition_variable>
#include <iostream>
#include <mutex>
#include <vector>
#include <thread>

#define READ 1
#define WRITE 2
#define EXIT 3

using namespace std;

int cnt_readers = 0;
int lft_readers = 0;
int cnt_writers = 0;

mutex mu;
condition_variable cond_empty;
condition_variable cond_full;

void readf()
{
	do
	{
		unique_lock<mutex> uLock(mu);
		cond_full.wait(uLock);

		cout << "Reading from file..." << --lft_readers << " readers left." << endl;
		if (lft_readers == 0)
		{
			lft_readers = cnt_readers;
			cond_empty.notify_one();
		}
	} while (cnt_writers != 0);
}

void writef()
{
	unique_lock<mutex> uLock(mu);
	cond_empty.wait(uLock);

	cout << "Writing to file..." << --cnt_writers << " writers left." <<endl;
	cond_full.notify_all();
}

int main()
{
	vector<thread> threads;
	cout << "Enter number of Readers: ";
	cin >> cnt_readers;
	cout << "Enter number of Writers: ";
	cin >> cnt_writers;

	lft_readers = cnt_readers;

	for (int i = 0; i < cnt_readers; i++)
		threads.push_back(thread(readf));

	for (int i = 0; i < cnt_writers; i++)
		threads.push_back(thread(writef));

	cond_empty.notify_one();
	for (size_t i = 0; i < threads.size(); i++)
		threads[i].join();
}